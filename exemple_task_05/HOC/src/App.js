import React from "react";
import withPostData from "./ContainerComponent/PostData";
import PostList from "./ViewComponent/PostList";
import PostTable from "./ViewComponent/PostTable";

// Wrap ProductList, ProductTable to get the higher order components
const PostListWithData = withPostData(PostList);
const PostTableWithData = withPostData(PostTable);

function PostRender() {
  return (
    <div>
      <PostListWithData />
      <PostTableWithData />
    </div>
  );
}

export default PostRender;
