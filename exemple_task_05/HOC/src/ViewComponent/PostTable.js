import React from "react";

const PostTable = ({ post, handleDelete }) => {
	if (!post) {
		return <div>Connecting...</div>;
	}
	return (
		<div className="table-container">
			<table className="table">
				<thead className="thead-dark">
					<tr>
						<th scope="col">id</th>
						<th scope="col">Email</th>
						<th scope="col">Post</th>
					</tr>
				</thead>
				<tbody>
					{post.map(post => (
						<tr>
								<th scope="row">{post.id}</th>
								<td>{post.email}</td>
								<td>{post.body}</td>
						</tr>
					))}
				</tbody>
			</table>	
		</div>
	);
};

export default PostTable;