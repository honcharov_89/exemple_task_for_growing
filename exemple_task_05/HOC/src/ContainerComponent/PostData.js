import React from "react";

const withPostData = WrappedComponent =>
  class PostData extends React.Component {
    constructor() {
      super();
      this.state = { post: [] };
    }

    componentDidMount() {
      this.getData();
    }

    getData = async () => {
      const response = await fetch(
        "https://jsonplaceholder.typicode.com/comments?postId=1"
      );
      const json = await response.json();
      this.setState({ post: json });
    };

    handleDelete = currentPost => {
      this.setState({
        post: this.state.post.filter(post => post.id !== currentPost.id)
      });
    };

    render() {
      return (
        <WrappedComponent
          post={this.state.post}
          handleDelete={this.handleDelete}
        />
      );
    }
  };

export default withPostData;
