import React from "react";

const PostList = ({ post, handleDelete }) => {
  if (!post) {
    return <div>Connecting...</div>;
  }
  return (
    <div>
      <div className="post">
        {post.map(post => (
          <div className="post-block">
            <span>{post.id}</span>
            <h2 className="post-block__autor">{post.email}</h2>
            <p className="post-block__text">{post.body}</p>
            <div>
              <button
                onClick={() => handleDelete(post)}
                className="btn btn-danger btn-position__rb"
              >
                Delete
              </button>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default PostList;
