import React from "react";
import PostData from "./ContainerComponent/PostData";
import PostList from "./ViewComponent/PostList";
import PostTable from "./ViewComponent/PostTable";

function PostRender() {
  return (
    <div>
      <PostData
        children={({ post, handleDelete }) => (
          <PostList post={post} handleDelete={handleDelete} />
        )}
      />

      <PostData>
        {({ post }) => {
          return <PostTable post={post} />;
        }}
      </PostData>
    </div>
  );
}

export default PostRender;
