import React from "react";

class PostData extends React.Component {
  constructor() {
    super();
    this.state = { post: [] };
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {
    const response = await fetch(
      "https://jsonplaceholder.typicode.com/comments?postId=1"
    );
    const json = await response.json();
    this.setState({ post: json });
  };

  handleDelete = currentPost => {
    this.setState({
      post: this.state.post.filter(post => post.id !== currentPost.id)
    });
  };

  render() {
    if (!this.state.post.length) return null;
    return this.props.children({
      post: this.state.post,
      handleDelete: this.handleDelete
    });
  }
}

export default PostData;
