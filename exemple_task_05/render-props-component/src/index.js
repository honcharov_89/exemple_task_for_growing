import React from "react";
import ReactDOM from "react-dom";
import "./styles.css";
import PostRender from "./App";

function renderComponent(RenderComponent, target) {
  return ReactDOM.render(<RenderComponent />, document.getElementById(target));
}

renderComponent(PostRender, "root");
