import React, { useState, useEffect } from 'react'

const Effect = () => {
  let [names, setNames] = useState([]);

  useEffect(() => {
    actionSideEffect();
    return () => {
      actionSideEffect();
    }
  }, []);

  const actionSideEffect = async () => {
    const response = await fetch("https://uinames.com/api/?amount=10&region=nigeria");
    const json = await response.json();
    setNames(json)
  }

  return (
    <div className="app">
      <div className="app-names">
        {names.map((item, i) => (
          <div key={i}>
            {item.name} {item.surname}
          </div>
        ))}
      </div>
    </div>
  );
}

export default Effect;