import React, { useState, useEffect } from 'react'

const Effect = () => {
  const [state, setState] = useState({counter:0});

  // // Вызов без второго параметра
  useEffect(() => {
    console.log('Я буду запускаться после каждого рендера');
  });

  // Со вторым параметром
  useEffect(() => {
    console.log('Я больше не вызовусь после первого рендера');
  }, []);

  // Со вторым параметром
    useEffect(() => {
      console.log('Я вызовусь только при изменении state');
    }, [state]);
  
  const add1ToCounter = () => {
    const newCounterValue = state.counter + 1;
    setState({ counter: newCounterValue});
  }

  return (
    <div>
      <p>You clicked {state.counter} times</p>
      <button onClick={add1ToCounter}>
        Click me
      </button>
    </div>
  );
}

export default Effect;