import React from "react";

export default class CommentListClass extends React.Component {
	render() {
		const { surname } = this.props;
		if (!surname) {
			return <div>Connecting...</div>;
		}
		return <h1>Hello, {surname}</h1>;
	}
}
