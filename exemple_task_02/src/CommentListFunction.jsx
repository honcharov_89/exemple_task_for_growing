import React from "react";

const CommentlistFunction = ({ name }) => {
	if (!name) {
		return <div>Connecting...</div>;
	}
	return <h1>Hi, {name}!</h1>;
};

export default CommentlistFunction;
