import React from "react";
import ReactDOM from "react-dom";

import CommentlistFunction from "./CommentlistFunction";
import CommentListClass from "./CommentListClass";

import "./styles.css";

class CommentListContainer extends React.Component {
	constructor() {
		super();
		this.state = {
			name: "Boris",
			surname: "Pupkin"
		};
	}

	componentDidMount() {
		this.setState({
			name: "Kolya",
			surname: "Bubkin"
		});
	}

	render() {
		return (
			<div>
				<CommentlistFunction name={this.state.name} />
				<CommentListClass surname={this.state.surname} />
			</div>
		);
	}
}

const rootElement = document.getElementById("root");
ReactDOM.render(<CommentListContainer />, rootElement);
