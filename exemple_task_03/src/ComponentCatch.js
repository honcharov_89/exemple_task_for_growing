import React from 'react';

class ShowMyError extends React.Component {
	constructor(props) {
		super(props);
		this.state = { error: false };
	}

	componentDidCatch(error, info) {
		this.setState({ error, info });
	}

	render() {
		if (this.state.error) {
			return (
				<div>
					<h1>
						Error msg
					</h1>
				</div>
			);
		}
		return this.props.children;
	}
}

class Broken extends React.Component {
	constructor(props) {
		super(props);
		this.state = { throw: false, count: 0 };
	}

	render() {
		if (this.state.throw) {
			throw new Error("YOLO");
		}

		return (
			<div>
				<button
					onClick={e => {
						this.setState({ throw: true });
					}}
				>
					button with error.
				</button>

				<button onClick={e => {
					this.setState(({ count }) => ({
						count: count + 1
					}));
				}}>button without error</button>

				<div>
					{"All good here. Count: "}{this.state.count}
				</div>
			</div>
		);
	}
}

class ComponentCatch extends React.Component {
	render() {
		return (
			<div >
				<ShowMyError>
					<Broken />
				</ShowMyError>
			</div>
		);
	}
}


export default ComponentCatch
