import React from 'react';

class LifeCicleNew extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			class: "off",
			label: "Нажми",
			name: this.props.name,
			newName: '',
		};

		this.press = this.press.bind(this);

		console.log("constructor");
	}
	static getDerivedStateFromProps(nextProps, prevState){
		console.log("getDerivedStateFromProps");
		if (prevState.name === nextProps.name) {
			return {
				name: nextProps.name,
				newName: nextProps.name + 'new'
			}
		}
		return null;
	}
	componentDidMount(){
		console.log("componentDidMount()");
		// can make fetch or refs ...
	}
	shouldComponentUpdate(nextProps, nextState){
		console.log("shouldComponentUpdate()");
		if (this.props !== nextProps) {
			return false;
		} else if (this.state !== nextState) {
			return true;
		}
	}
	getSnapshotBeforeUpdate(prevProps, prevState){
		console.log("getSnapshotBeforeUpdate")
		return null;
	}
	componentDidUpdate(prevProps, prevState, snapshot){
		console.log("componentDidUpdate()");
		if (this.props.name !== prevProps.name) {
			// can make setState and requests but must have condition
		}
	}
	componentWillUnmount(){
		console.log("componentWillUnmount()");
		// cancel setInterval with clearInterval
		// cancel Events
		// cancel fetch ...
	}
	press(){
		var className = (this.state.class==="off")?"on":"off";
		this.setState({class: className});
	}
	render() {
		console.log("render()");
		return <button onClick={this.press} className={this.state.class}>{this.state.label}</button>;
	}
}
export default LifeCicleNew