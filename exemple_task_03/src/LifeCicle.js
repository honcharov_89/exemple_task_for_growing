import React from 'react';

class LifeCicle extends React.Component {
	constructor(props) {
		super(props);
		this.state = {class: "off", label: "Нажми"};

		this.press = this.press.bind(this);

		console.log("constructor");
	}
	UNSAFE_componentWillReceiveProps(nextProps) {
		console.log("UNSAFE_componentWillReceiveProps()");
	}
	UNSAFE_componentWillMount(){
		console.log("UNSAFE_componentWillMount()");
	}
	componentDidMount(){
		console.log("componentDidMount()");
	}
	shouldComponentUpdate(){
		console.log("shouldComponentUpdate()");
		return true;
	}
	UNSAFE_componentWillUpdate(){
		console.log("UNSAFE_componentWillUpdate()");
	}
	componentDidUpdate(){
		console.log("componentDidUpdate()");
	}
	componentWillUnmount(){
		console.log("componentWillUnmount()");
	}
	press(){
		var className = (this.state.class==="off")?"on":"off";
		this.setState({class: className});
	}
	render() {
		console.log("render()");
		return <button onClick={this.press} className={this.state.class}>{this.state.label}</button>;
	}
}
export default LifeCicle